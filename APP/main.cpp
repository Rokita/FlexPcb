#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include <APP/Application/Application.hpp>

#include <BSP/BSP.hpp>

int main(void)
   {
   oBSP.init();
   sei();
   while(1)
      {
      oApp.update();
      _delay_ms(10);
      }
   }
