/*
 * StateProgram_1.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_STATES_STATEPROGRAM_1_HPP_
#define APP_APPLICATION_STATES_STATEPROGRAM_1_HPP_

#include <APP/Application/StateMachine/State.hpp>

class StateProgram_1: public State
   {
public:
   StateProgram_1();

   virtual void enter();
   virtual void update();
   virtual void exit();

   virtual void Button_1_Pressed();
   virtual void Button_2_Pressed();

   virtual void Button_1_Released();
   virtual void Button_2_Released();

private:
   void invalidate();
   uint8_t index;
   };

#endif /* APP_APPLICATION_STATES_STATEPROGRAM_1_HPP_ */
