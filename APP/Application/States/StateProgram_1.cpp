/*
 * StateProgram_1.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <APP/Application/States/StateProgram_1.hpp>
#include <APP/Application/Application.hpp>

uint8_t displayTensSnakeValues[6] =
   {
   0x01, 0x02, 0x04, 0x08, 0x10, 0x20
   };
uint8_t displayUnitsSnakeValues[6] =
   {
   0x01, 0x20, 0x10, 0x08, 0x04, 0x02
   };
uint8_t ledsValues[6] =
   {
   0x01, 0x02, 0x04, 0x01, 0x02, 0x04
   };
StateProgram_1::StateProgram_1()
   {
   index = 0;
   }

void StateProgram_1::enter()
   {
   pApp->pData->currentActionTimeMsIndex = eActionTimeMs_2000;
   pApp->pBSP->ApplicationTimer_setTime(pApp->pData->actionTimeMs[pApp->pData->currentActionTimeMsIndex]);
   pApp->pBSP->ApplicationTimer_start();
   index = 0;
   invalidate();
   }
void StateProgram_1::update()
   {
   if(pApp->pBSP->ApplicationTimer_isExpired() == true)
      {
      pApp->pBSP->ApplicationTimer_reset();
      pApp->pBSP->ApplicationTimer_start();
      index = (index + 1) % 6;
      invalidate();
      }
   }
void StateProgram_1::exit()
   {
   pApp->pBSP->ApplicationTimer_stop();
   }

void StateProgram_1::Button_1_Pressed()
   {
   Sound *sound = &pApp->pData->buzzer.sound[eSoundType_Transition];
   pApp->pBSP->Buzzer_GenerateFrequency(sound->frequency, sound->time);
   pApp->oSM.transitionTo(&(pApp->stateProgram_2));
   }
void StateProgram_1::Button_2_Pressed()
   {
   ActionTimeMs *currentIndex = &(pApp->pData->currentActionTimeMsIndex);
   *currentIndex = (ActionTimeMs) ((*currentIndex + 1) % eActionTimeMs_NumOf);
   pApp->pBSP->ApplicationTimer_setTime(pApp->pData->actionTimeMs[*currentIndex]);
   Sound *sound = &pApp->pData->buzzer.sound[eSoundType_Switching];
   pApp->pBSP->Buzzer_GenerateFrequency(sound->frequency, sound->time);
   }

void StateProgram_1::Button_1_Released()
   {
   }
void StateProgram_1::Button_2_Released()
   {
   }

void StateProgram_1::invalidate()
   {
   pApp->pBSP->DisplayTens_setSegments(displayTensSnakeValues[index]);
   pApp->pBSP->DisplayUnits_setSegments(displayUnitsSnakeValues[index]);
   (ledsValues[index] & 0x01) ? pApp->pBSP->Led1_on() : pApp->pBSP->Led1_off();
   (ledsValues[index] & 0x02) ? pApp->pBSP->Led2_on() : pApp->pBSP->Led2_off();
   (ledsValues[index] & 0x04) ? pApp->pBSP->Led14_on() : pApp->pBSP->Led14_off();
   }
