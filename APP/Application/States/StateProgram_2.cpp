/*
 * StateProgram_2.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <APP/Application/States/StateProgram_2.hpp>
#include <APP/Application/Application.hpp>

StateProgram_2::StateProgram_2()
   {
   tensDP_On = false;
   displayedValue = 0;
   }
void StateProgram_2::enter()
   {
   pApp->pData->currentActionTimeMsIndex = eActionTimeMs_2000;
   pApp->pBSP->ApplicationTimer_setTime(pApp->pData->actionTimeMs[pApp->pData->currentActionTimeMsIndex]);
   pApp->pBSP->ApplicationTimer_start();
   tensDP_On = false;
   displayedValue = 0;
   invalidate();
   }
void StateProgram_2::update()
   {
   if(pApp->pBSP->ApplicationTimer_isExpired() == true)
      {
      displayedValue++;
      if(displayedValue > 99)
         {
         displayedValue = 0;
         tensDP_On = !tensDP_On;
         }
      pApp->pBSP->ApplicationTimer_reset();
      pApp->pBSP->ApplicationTimer_start();
      invalidate();
      }
   }
void StateProgram_2::exit()
   {
   pApp->pBSP->ApplicationTimer_stop();
   }

void StateProgram_2::Button_1_Pressed()
   {
   Sound *sound = &pApp->pData->buzzer.sound[eSoundType_Transition];
   pApp->pBSP->Buzzer_GenerateFrequency(sound->frequency, sound->time);
   pApp->oSM.transitionTo(&(pApp->stateProgram_1));
   }

void StateProgram_2::Button_2_Pressed()
   {
   ActionTimeMs *currentIndex = &(pApp->pData->currentActionTimeMsIndex);
   *currentIndex = (ActionTimeMs) ((*currentIndex + 1) % eActionTimeMs_NumOf);
   pApp->pBSP->ApplicationTimer_setTime(pApp->pData->actionTimeMs[*currentIndex]);
   Sound *sound = &pApp->pData->buzzer.sound[eSoundType_Switching];
   pApp->pBSP->Buzzer_GenerateFrequency(sound->frequency, sound->time);
   }

void StateProgram_2::Button_1_Released()
   {
   }
void StateProgram_2::Button_2_Released()
   {
   }

void StateProgram_2::invalidate()
   {
   uint8_t *numbers = pApp->pData->display7Seg.number;
   uint8_t tens = numbers[displayedValue / 10];
   if(tensDP_On)
      {
      tens |= (1 << 7);
      }
   uint8_t units = numbers[displayedValue % 10];
   pApp->pBSP->DisplayTens_setSegments(tens);
   pApp->pBSP->DisplayUnits_setSegments(units);
   pApp->pBSP->Led1_on();
   pApp->pBSP->Led2_on();
   pApp->pBSP->Led14_on();
   }
