/*
 * StateTemplate.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <APP/Application/States/Template/StateTemplate.hpp>
#include <APP/Application/Application.hpp>

StateTemplate::StateTemplate() :
      State()
   {
   }
void StateTemplate::enter()
   {
   }
void StateTemplate::update()
   {
   }
void StateTemplate::exit()
   {
   }
