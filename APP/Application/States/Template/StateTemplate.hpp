/*
 * StateTemplate.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_STATES_STATETEMPLATE_HPP_
#define APP_APPLICATION_STATES_STATETEMPLATE_HPP_

#include <APP/Application/StateMachine/State.hpp>

class StateTemplate: public State
   {
public:
   StateTemplate();

   void enter();
   void update();
   void exit();
private:

   };

#endif /* APP_APPLICATION_STATES_STATETEMPLATE_HPP_ */
