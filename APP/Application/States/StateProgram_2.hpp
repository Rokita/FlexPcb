/*
 * StateProgram_2.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_STATES_STATEPROGRAM_2_HPP_
#define APP_APPLICATION_STATES_STATEPROGRAM_2_HPP_

#include <APP/Application/StateMachine/State.hpp>

class StateProgram_2: public State
   {
public:
   StateProgram_2();

   virtual void enter();
   virtual void update();
   virtual void exit();

   virtual void Button_1_Pressed();
   virtual void Button_2_Pressed();

   virtual void Button_1_Released();
   virtual void Button_2_Released();
private:
   void invalidate();
   bool tensDP_On;
   uint8_t displayedValue;
   };

#endif /* APP_APPLICATION_STATES_STATEPROGRAM_2_HPP_ */
