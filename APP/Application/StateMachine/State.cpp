/*
 * State.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <APP/Application/StateMachine/State.hpp>
#include <App/Application/Application.hpp>

#include <BSP/BSP.hpp>

Application *State::pApp = (Application*) 0;

void State::enter()
   {
   }
void State::update()
   {
   }
void State::exit()
   {
   }
void State::Button_1_Pressed()
   {
   }
void State::Button_2_Pressed()
   {
   }
void State::Button_1_Released()
   {
   }
void State::Button_2_Released()
   {
   }
