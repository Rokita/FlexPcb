/*
 * State.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_STATEMACHINE_STATE_HPP_
#define APP_APPLICATION_STATEMACHINE_STATE_HPP_

extern "C"
{
#include <stdint.h>
}

class Application;

class State
   {
public:

   virtual void enter();
   virtual void update();
   virtual void exit();

   virtual void Button_1_Pressed();
   virtual void Button_2_Pressed();

   virtual void Button_1_Released();
   virtual void Button_2_Released();

   static Application *pApp;
private:
   };

#endif /* APP_APPLICATION_STATEMACHINE_STATE_HPP_ */
