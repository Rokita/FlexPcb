/*
 * StateMachine.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_STATEMACHINE_STATEMACHINE_HPP_
#define APP_APPLICATION_STATEMACHINE_STATEMACHINE_HPP_

#include <APP/Application/StateMachine/State.hpp>
#include <BSP/BSP.hpp>

typedef enum Step
   {
   eStep_update = 0,
   eStep_transition,
   eStep_LastUnused
   } Step_T;

class StateMachine
   {
public:
   StateMachine(State *pState)
      {
      pCurrentState = pNextState = (State *) 0;
      transitionTo(pState);
      }
   ;
   ~StateMachine()
      {
      }
   ;
   void update()
      {
//      oBSP.Led1_toggle();
      switch(eStep)
         {
         case eStep_update:
//            oBSP.Led2_toggle();
            pCurrentState->update();
            break;
         case eStep_transition:
//            oBSP.Led3_toggle();
            if(pCurrentState != (State*) 0)
               {
               pCurrentState->exit();
               }
            pCurrentState = pNextState;
            pCurrentState->enter();
            eStep = eStep_update;
            break;
         default:
            break;
         }
      }
   void transitionTo(State *pNewState)
      {
      pNextState = pNewState;
      eStep = eStep_transition;
      }
   ;
   State *currentState(void)
      {
      return pCurrentState;
      }
private:
   State *pNextState;
   State *pCurrentState;
   Step_T eStep;
   };

#endif /* APP_APPLICATION_STATEMACHINE_STATEMACHINE_HPP_ */
