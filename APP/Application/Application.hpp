/*
 * Application.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef APP_APPLICATION_APPLICATION_HPP_
#define APP_APPLICATION_APPLICATION_HPP_

#include <BSP/BSP.hpp>
#include <Data/DataStructure.hpp>

#include <APP/Application/StateMachine/StateMachine.hpp>
#include <APP/Application/States/StateProgram_1.hpp>
#include <APP/Application/States/StateProgram_2.hpp>


class Application
   {
public:
   Application();
   void update();

   BSP *pBSP;
   DataStructure *pData;
   StateMachine oSM;

   StateProgram_1 stateProgram_1;
   StateProgram_2 stateProgram_2;
private:
   void checkButtons();
   };

extern Application oApp;

#endif /* APP_APPLICATION_APPLICATION_HPP_ */
