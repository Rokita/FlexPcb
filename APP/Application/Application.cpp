/*
 * Application.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <APP/Application/Application.hpp>

#include <BSP/BSP.hpp>
#include <Data/DataStructure.hpp>

Application oApp;

Application::Application() :
      oSM(&stateProgram_1)
   {
   pBSP = &oBSP;
   pData = &oDataStructure;
   State::pApp = this;
   }
void Application::update()
   {
   checkButtons();
   oSM.update();
   }


void Application::checkButtons()
   {
   if(pBSP->Button1_isPressed())
      {
      oSM.currentState()->Button_1_Pressed();
      }
   if(pBSP->Button1_isReleased())
      {
      oSM.currentState()->Button_1_Released();
      }
   if(pBSP->Button2_isPressed())
      {
      oSM.currentState()->Button_2_Pressed();
      }
   if(pBSP->Button2_isReleased())
      {
      oSM.currentState()->Button_2_Released();
      }
   }
