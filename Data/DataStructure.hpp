/*
 * DataStructure.hpp
 *
 *  Created on: 24.03.2018
 *      Author: Kamil
 */

#ifndef DATA_DATASTRUCTURE_HPP_
#define DATA_DATASTRUCTURE_HPP_

#include <Data/Display/Display7Seg.hpp>
#include <Data/Buzzer/Buzzer.hpp>

enum ActionTimeMs
   {
   eActionTimeMs_2000 = 0,
   eActionTimeMs_1000,
   eActionTimeMs_500,
   eActionTimeMs_100,
   eActionTimeMs_NumOf
   };

struct DataStructure
   {
   Display7Seg display7Seg;
   Buzzer buzzer;
   uint16_t *actionTimeMs;
   ActionTimeMs currentActionTimeMsIndex;
   };

extern DataStructure oDataStructure;


#endif /* DATA_DATASTRUCTURE_HPP_ */
