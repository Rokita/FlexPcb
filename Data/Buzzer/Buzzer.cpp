/*
 * Buzzer.cpp
 *
 *  Created on: 25.03.2018
 *      Author: Kamil
 */

#include <Data/Buzzer/Buzzer.hpp>

#define TRANSITION_FREQUENCY (2700)
#define TRANSITION_TIME (1000)
Sound transition =
   {
   TRANSITION_FREQUENCY,
   TRANSITION_TIME
   };

#define SWITCHING_FREQUENCY (6500)
#define SWITCHING_TIME (2000)
Sound switching =
   {
   SWITCHING_FREQUENCY,
   SWITCHING_TIME
   };

Sound sounds[eSoundType_NumOf] =
      {
       transition,
       switching
      };

/*
 * externed object
 */
Buzzer oBuzzer =
   {
    sounds
   };

