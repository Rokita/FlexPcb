/*
 * Buzzer.hpp
 *
 *  Created on: 25.03.2018
 *      Author: Kamil
 */

#ifndef DATA_BUZZER_BUZZER_HPP_
#define DATA_BUZZER_BUZZER_HPP_

extern "C"
   {
#include <stdint.h>
   }

enum SoundType
   {
   eSoundType_Transition = 0,
   eSoundType_Switching,
   eSoundType_NumOf
   };

struct Sound
   {
   uint32_t frequency;
   uint16_t time;
   };

struct Buzzer
   {
   Sound *sound;
   };

extern Buzzer oBuzzer;

#endif /* DATA_BUZZER_BUZZER_HPP_ */
