/*
 * Display7Seg.hpp
 *
 *  Created on: 24.03.2018
 *      Author: Kamil
 */

#ifndef DATA_DISPLAY_DISPLAY7SEG_HPP_
#define DATA_DISPLAY_DISPLAY7SEG_HPP_

extern "C"
{
#include <stdint.h>
}

enum DisplayNumber
   {
   eDisplayNumber_0 = 0,
   eDisplayNumber_1,
   eDisplayNumber_2,
   eDisplayNumber_3,
   eDisplayNumber_4,
   eDisplayNumber_5,
   eDisplayNumber_6,
   eDisplayNumber_7,
   eDisplayNumber_8,
   eDisplayNumber_9,
   eDisplayNumber_NumOf
   };

struct Display7Seg
   {
   uint8_t *number;
   };

extern Display7Seg oDisplay7seg;

#endif /* DATA_DISPLAY_DISPLAY7SEG_HPP_ */
