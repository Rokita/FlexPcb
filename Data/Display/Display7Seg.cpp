/*
 * Display7Seg.cpp
 *
 *  Created on: 24.03.2018
 *      Author: Kamil
 */

#include <Data/Display/Display7Seg.hpp>

#define VAL_0           (0x3F)
#define VAL_1           (0x06)
#define VAL_2           (0x5B)
#define VAL_3           (0x4F)
#define VAL_4           (0x66)
#define VAL_5           (0x6D)
#define VAL_6           (0x7D)
#define VAL_7           (0x07)
#define VAL_8           (0x7F)
#define VAL_9           (0x6F)

uint8_t displayNumber[eDisplayNumber_NumOf] =
   {
   VAL_0,
   VAL_1,
   VAL_2,
   VAL_3,
   VAL_4,
   VAL_5,
   VAL_6,
   VAL_7,
   VAL_8,
   VAL_9
   };

Display7Seg oDisplay7seg =
   {
   displayNumber
   };
