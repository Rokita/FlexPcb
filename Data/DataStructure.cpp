/*
 * DataStructure.cpp
 *
 *  Created on: 24.03.2018
 *      Author: Kamil
 */

#include <Data/DataStructure.hpp>

#define DISPLAY_NUMBERS         (oDisplay7seg)
#define BUZZER_DATA             (oBuzzer)

uint16_t actionTimeMs[eActionTimeMs_NumOf] =
      {
       2000,
       1000,
       500,
       100
      };
ActionTimeMs currentActionTimeMsIndex = eActionTimeMs_2000;

DataStructure oDataStructure =
   {
   DISPLAY_NUMBERS,
   BUZZER_DATA,
   actionTimeMs,
   currentActionTimeMsIndex
   };

