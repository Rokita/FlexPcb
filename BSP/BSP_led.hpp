/*
 * BSP_led.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_LED_HPP_
#define BSP_BSP_LED_HPP_

extern "C"
   {
#include <stdint.h>
   }

typedef enum LedState
   {
   eLedState_Off,
   eLedState_On,
   eLedState_blinkOff,
   eLedState_blinkOn
   } LedState_T;

template<typename T_PIN>
class Led
   {
public:
   Led();
   void update(uint16_t time_ms);
   void on();
   void off();
   void toggle();
   void blink(uint16_t onTime_ms, uint16_t offTime_ms);
private:
   T_PIN ledPin;
   LedState_T state;
   uint16_t elapsedTime_ms;
   uint16_t blinkOffTime_ms;
   uint16_t blinkOnTime_ms;
   };

/*************************************
 *              DEFINITION
 *************************************/

template<typename T_PIN>
Led<T_PIN>::Led()
   {
   ledPin.set();
   state = eLedState_Off;
   elapsedTime_ms = 0;
   blinkOffTime_ms = 0;
   blinkOnTime_ms = 0;
   }

template<typename T_PIN>
void Led<T_PIN>::update(uint16_t time_ms)
   {
   switch(state)
      {
      case eLedState_Off:
         ledPin.set();
         break;
      case eLedState_On:
         ledPin.reset();
         break;
      case eLedState_blinkOff:
         ledPin.set();
         elapsedTime_ms += time_ms;
         if(elapsedTime_ms > blinkOffTime_ms)
            {
            state = eLedState_blinkOn;
            elapsedTime_ms = 0;
            }
         break;
      case eLedState_blinkOn:
         ledPin.reset();
         elapsedTime_ms += time_ms;
         if(elapsedTime_ms > blinkOnTime_ms)
            {
            state = eLedState_blinkOff;
            elapsedTime_ms = 0;
            }
         break;
      default:
         break;
      }
   }

template<typename T_PIN>
void Led<T_PIN>::on()
   {
   state = eLedState_On;
   }

template<typename T_PIN>
void Led<T_PIN>::off()
   {
   state = eLedState_Off;
   }

template<typename T_PIN>
void Led<T_PIN>::toggle()
   {
   switch(state)
      {
      case eLedState_Off:
         state = eLedState_On;
         break;
      case eLedState_On:
         state = eLedState_Off;
         break;
      case eLedState_blinkOff:
         state = eLedState_blinkOn;
         elapsedTime_ms = 0;
         break;
      case eLedState_blinkOn:
         state = eLedState_blinkOff;
         elapsedTime_ms = 0;
         break;
      default:
         break;
      }
   }

template<typename T_PIN>
void Led<T_PIN>::blink(uint16_t onTime_ms, uint16_t offTime_ms)
   {
   blinkOffTime_ms = offTime_ms;
   blinkOnTime_ms = onTime_ms;
   if(state != eLedState_blinkOff && state != eLedState_blinkOn)
      {
      state = (state == eLedState_On) ? eLedState_blinkOff : eLedState_blinkOn;
      elapsedTime_ms = 0;
      }
   }

#endif /* BSP_BSP_LED_HPP_ */
