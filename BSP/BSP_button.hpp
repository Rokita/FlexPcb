/*
 * BSP_button.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_BUTTON_HPP_
#define BSP_BSP_BUTTON_HPP_

#include <avr/interrupt.h>

#define DEBOUNCING_TIME (50)

typedef enum ButtonState
   {
   eButtonState_None,
   eButtonState_Pressed,
   eButtonState_Released
   } ButtonState_T;

template<typename T_PIN>
class Button
   {
public:
   Button();
   void update(uint8_t time_ms);
   bool isPressed();
   bool isReleased();
private:
   T_PIN buttonPin;
   uint8_t pressedTime_ms;
   uint8_t releasedTime_ms;
   ButtonState_T state;
   bool stateChanged;
   };

/*************************************
 *              DEFINITION
 *************************************/
template<typename T_PIN>
Button<T_PIN>::Button()
   {
   pressedTime_ms = 0;
   releasedTime_ms = 0;
   state = eButtonState_None;
   stateChanged = false;
   }

template<typename T_PIN>
void Button<T_PIN>::update(uint8_t time_ms)
   {
   if(buttonPin.read() == false) // button pressed
      {
      releasedTime_ms = 0;
      if(state != eButtonState_Pressed)
         {
         pressedTime_ms += time_ms;
         if(pressedTime_ms >= DEBOUNCING_TIME)
            {
            pressedTime_ms = 0;
            state = eButtonState_Pressed;
            stateChanged = true;
            }
         }
      }
   else
      {
      pressedTime_ms = 0;
      if(state == eButtonState_Pressed)
         {
         releasedTime_ms += time_ms;
         if(releasedTime_ms >= DEBOUNCING_TIME)
            {
            releasedTime_ms = 0;
            state = eButtonState_Released;
            stateChanged = true;
            }
         }
      }
   }

template<typename T_PIN>
bool Button<T_PIN>::isPressed()
   {
   if(stateChanged == true && state == eButtonState_Pressed)
      {
      stateChanged = false;
      return true;
      }
   return false;
   }

template<typename T_PIN>
bool Button<T_PIN>::isReleased()
   {
   if(stateChanged == true && state == eButtonState_Released)
      {
      stateChanged = false;
      return true;
      }
   return false;
   }
#endif /* BSP_BSP_BUTTON_HPP_ */
