/*
 * BSP_SoftwareTimer.hpp
 *
 *  Created on: 26.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_SOFTWARETIMER_HPP_
#define BSP_BSP_SOFTWARETIMER_HPP_

typedef enum TimerState
   {
   eTimerState_Stop,
   eTimerState_Running,
   eTimerState_Expired,

   } TimerState_T;

class SoftwareTimer
   {
public:
   SoftwareTimer();
   void start();
   void stop();
   void reset();
   void update(uint16_t time);
   void setTime(uint16_t time);
   bool isExpired();
private:
   void init();
   uint16_t elapsedTime;
   uint16_t expiredTime;
   TimerState_T eState;
   };
SoftwareTimer::SoftwareTimer()
   {
   eState = eTimerState_Stop;
   elapsedTime = 0;
   expiredTime = 0;
   }
void SoftwareTimer::start()
   {
   eState = eTimerState_Running;
   elapsedTime = 0;
   }
void SoftwareTimer::stop()
   {
   eState = eTimerState_Stop;
   }
void SoftwareTimer::reset()
   {
   elapsedTime = 0;
   start();
   }
void SoftwareTimer::update(uint16_t time)
   {
   if(eState == eTimerState_Running)
      {
      if(elapsedTime >= expiredTime)
         {
         eState = eTimerState_Expired;
         }
      else
         {
         elapsedTime += time;
         }
      }
   }
void SoftwareTimer::setTime(uint16_t time)
   {
   expiredTime = time;
   }
bool SoftwareTimer::isExpired()
   {
   return (eState == eTimerState_Expired);
   }

#endif /* BSP_BSP_SOFTWARETIMER_HPP_ */
