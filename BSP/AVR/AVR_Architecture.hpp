/*
 * AVR_Architecture.hpp
 *
 *  Created on: 23.03.2018
 *      Author: Kamil
 */

#ifndef BSP_ARCHITECTURE_AVR_AVR_ARCHITECTURE_HPP_
#define BSP_ARCHITECTURE_AVR_AVR_ARCHITECTURE_HPP_

#include <BSP/Architecture/Architecture.hpp>
#include <BRD/AVR/gpio.h>

namespace AVR_Architecture
   {
   template<GpioPort_T port, GpioPin_T pin>
   class GpioInput: public Architecture::GpioInput
      {
   public:
      GpioInput() :
            Architecture::GpioInput()
         {
         Gpio_Pin_Config_Input(port, pin);
         Gpio_Pin_PullUp(port, pin);
         }
      bool read()
         {
         return (Gpio_Pin_Read(port, pin) == eGpioPinState_High);
         }
      };

   template<GpioPort_T port, GpioPin_T pin>
   class GpioOutput: public Architecture::GpioOutput
      {
   public:
      GpioOutput() :
            Architecture::GpioOutput()
         {
         Gpio_Pin_Config_Output(port, pin);
         }
      void set()
         {
         Gpio_Pin_Set(port, pin);
         }
      void reset()
         {
         Gpio_Pin_Reset(port, pin);
         }
      void toggle()
         {
         Gpio_Pin_Toggle(port, pin);
         }
      bool isSet()
         {
         return (Gpio_Pin_Read(port, pin) == eGpioPinState_High);
         }
      };

   template<GpioPort_T port, GpioPin_T pin>
   class GpioSpecial: public Architecture::GpioSpecial
      {
   public:
      GpioSpecial() :
            Architecture::GpioSpecial()
         {

         }
      };

   template<GpioPort_T port, GpioPin_T pin>
   class GpioUnused: public Architecture::GpioSpecial
      {
   public:
      GpioUnused() :
            Architecture::GpioUnused()
         {

         }
      };

   class Timer:public Architecture::Timer
      {
      public:
      Timer():Architecture::Timer()
         {
         }
      };
   }


#endif /* BSP_ARCHITECTURE_AVR_AVR_ARCHITECTURE_HPP_ */
