/*
 * BSP.cpp
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include <BSP/BSP.hpp>

#include <BSP/AVR/AVR_Gpio.hpp>
#include <BSP/AVR/AVR_Timer.hpp>
#include <BSP/BSP_button.hpp>
#include <BSP/BSP_led.hpp>
#include <BSP/BSP_Dispay_7seg.hpp>
#include <BSP/BSP_buzzer.hpp>
#include <BSP/BSP_SoftwareTimer.hpp>

#define P(port,pin) Gpio::Port##port::Pin_##pin
#define TICK_FREQUENCY (100)

BSP oBSP;
//
///* Buttons */
Button<P(C,5)> Button_SW1;
Button<P(C,4)> Button_SW2;
//
///* Leds*/
Led<P(C,1)> Led1;
Led<P(C,2)> Led2;
Led<P(C,3)> Led14;
//
///* Displays */
Display_7SEG<P(D,3), P(D,1), P(D,6), P(D,5), P(D,7), P(D,0), P(D,2), P(D,4), P(B,0)> Display_tens;
Display_7SEG<P(D,2), P(D,7), P(D,6), P(D,5), P(D,4), P(D,1), P(D,0), P(D,3), P(B,1)> Display_units;
//
///* Buzzers*/
Buzzer<P(C,0), Timer::Timer0::CTC > Buzzer1;
//
///* SoftwareTimers*/
SoftwareTimer ApplicationTimer;
//
///* Tick Generator */
Timer::Timer1::CTC TickGenerator;

BSP::BSP()
   {
   }

void BSP::init()
   {
   sei();
   TickGenerator.setFrequency(TICK_FREQUENCY);
   TickGenerator.start();
   }
void BSP::update(uint16_t time)
   {
   ApplicationTimer.update(time);
   Led1.update(time);
   Led2.update(time);
   Led14.update(time);
   Button_SW1.update(time);
   Button_SW2.update(time);
   Buzzer1.update(time);
   }
bool BSP::Button1_isPressed()
   {
   return Button_SW1.isPressed();
   }
bool BSP::Button1_isReleased()
   {
   return Button_SW1.isReleased();
   }
bool BSP::Button2_isPressed()
   {
   return Button_SW2.isPressed();
   }
bool BSP::Button2_isReleased()
   {
   return Button_SW2.isReleased();
   }
/* Leds */
void BSP::Led1_on()
   {
   Led1.on();
   }
void BSP::Led1_off()
   {
   Led1.off();
   }
void BSP::Led1_toggle()
   {
   Led1.toggle();
   }
void BSP::Led1_blink(uint16_t onTime_ms, uint16_t offTime_ms)
   {
   Led1.blink(onTime_ms, offTime_ms);
   }
void BSP::Led2_on()
   {
   Led2.on();
   }
void BSP::Led2_off()
   {
   Led2.off();
   }
void BSP::Led2_toggle()
   {
   Led2.toggle();
   }
void BSP::Led2_blink(uint16_t onTime_ms, uint16_t offTime_ms)
   {
   Led2.blink(onTime_ms, offTime_ms);
   }
void BSP::Led14_on()
   {
   Led14.on();
   }
void BSP::Led14_off()
   {
   Led14.off();
   }
void BSP::Led14_toggle()
   {
   Led14.toggle();
   }
void BSP::Led14_blink(uint16_t onTime_ms, uint16_t offTime_ms)
   {
   Led14.blink(onTime_ms, offTime_ms);
   }
/* Displays */
void BSP::DisplayTens_setSegments(uint8_t segments)
   {
   Display_tens.setSegments(segments);
   }
void BSP::DisplayUnits_setSegments(uint8_t segments)
   {
   Display_units.setSegments(segments);
   }
/* Buzzers */
void BSP::Buzzer_GenerateFrequency(uint32_t freq, uint16_t time)
   {
   Buzzer1.generateFrequency(freq, time);
   }
void BSP::Buzzer_Stop()
   {
   Buzzer1.stop();
   }

/* Software Timers */

void BSP::ApplicationTimer_setTime(uint16_t time)
   {
   ApplicationTimer.setTime(time);
   }
void BSP::ApplicationTimer_start()
   {
   ApplicationTimer.start();
   }
void BSP::ApplicationTimer_stop()
   {
   ApplicationTimer.stop();
   }
void BSP::ApplicationTimer_reset()
   {
   ApplicationTimer.reset();
   }
bool BSP::ApplicationTimer_isExpired()
   {
   return ApplicationTimer.isExpired();
   }

/* Interrupt Handlers */

ISR(TIMER0_COMPA_vect)
   {
   Buzzer1.toggle();
   }
volatile bool DisplayTens_Active = false;
ISR(TIMER1_COMPA_vect)
   {
   oBSP.update(1000/TICK_FREQUENCY);
   if(DisplayTens_Active == true)
      {
      Display_tens.hide();
      Display_units.show();
      }
   else
      {
      Display_units.hide();
      Display_tens.show();
      }
   DisplayTens_Active = !DisplayTens_Active;
   }

ISR(BADISR_vect)
   {
   oBSP.Led14_toggle();
   }
