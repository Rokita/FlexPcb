/*
 * Gpio.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_ARCHITECTURE_AVR_TIMER_HPP_
#define BSP_ARCHITECTURE_AVR_TIMER_HPP_

#include <BSP/AVR/AVR_Architecture.hpp>

extern "C"
   {
#include <BRD/AVR/timer.h>
   }
static const uint32_t CPU_CLOCK = F_CPU;

namespace Timer
   {
   namespace Timer0
      {
      static const uint8_t TOP_VALUE = 0xFF;
      class CTC: public AVR_Architecture::Timer
         {
      public:
         CTC() :
               AVR_Architecture::Timer()
            {
            Timer0_setCompAMode_Disconected();
            Timer0_setCompBMode_Disconected();
            Timer0_setMode_CTC();
            }
         void start()
            {
            Timer0_enableInterrupt_CompA();
            }
         void stop()
            {
            Timer0_disableInterrupt_CompA();
            }
         void setFrequency(uint32_t freq)
            {
            if(freq == 0)
               {
               Timer0_setClockSpeed_None();
               return;
               }
            uint32_t value = (CPU_CLOCK - freq) / (freq);
            if(value > TOP_VALUE)
               {
               value = (CPU_CLOCK - freq) / (8 * freq);
               if(value > TOP_VALUE)
                  {
                  value = (CPU_CLOCK - freq) / (64 * freq);
                  if(value > TOP_VALUE)
                     {
                     value = (CPU_CLOCK - freq) / (256 * freq);
                     if(value > TOP_VALUE)
                        {
                        value = (CPU_CLOCK - freq) / (1024 * freq);
                        Timer0_set_OCRAValue(value);
                        Timer0_setClockSpeed_ClkDiv1024();
                        }
                     else
                        {
                        Timer0_set_OCRAValue(value);
                        Timer0_setClockSpeed_ClkDiv256();
                        }
                     }
                  else
                     {
                     Timer0_set_OCRAValue(value);
                     Timer0_setClockSpeed_ClkDiv64();
                     }
                  }
               else
                  {
                  Timer0_set_OCRAValue(value);
                  Timer0_setClockSpeed_ClkDiv8();
                  }
               }
            else
               {
               Timer0_set_OCRAValue(value);
               Timer0_setClockSpeed_Clk();
               }
            }
         };
      }

   namespace Timer1
      {
      static const uint16_t TOP_VALUE = 0xFFFF;
      class CTC: public AVR_Architecture::Timer
         {
      public:
         CTC() :
               AVR_Architecture::Timer()
            {
            Timer1_setCompAMode_Disconected();
            Timer1_setCompBMode_Disconected();
            Timer1_setMode_CTC();
            }
         void start()
            {
            Timer1_enableInterrupt_CompA();
            }
         void stop()
            {
            Timer1_disableInterrupt_CompA();
            }
         void setFrequency(uint32_t freq)
            {
            if(freq == 0)
               {
               Timer1_setClockSpeed_None();
               return;
               }
            uint32_t value = (CPU_CLOCK - freq) / (freq);
            if(value > TOP_VALUE)
               {
               value = (CPU_CLOCK - freq) / (8 * freq);
               if(value > TOP_VALUE)
                  {
                  value = (CPU_CLOCK - freq) / (64 * freq);
                  if(value > TOP_VALUE)
                     {
                     value = (CPU_CLOCK - freq) / (256 * freq);
                     if(value > TOP_VALUE)
                        {
                        value = (CPU_CLOCK - freq) / (1024 * freq);
                        Timer1_set_OCRAValue(value);
                        Timer1_setClockSpeed_ClkDiv1024();
                        }
                     else
                        {
                        Timer1_set_OCRAValue(value);
                        Timer1_setClockSpeed_ClkDiv256();
                        }
                     }
                  else
                     {
                     Timer1_set_OCRAValue(value);
                     Timer1_setClockSpeed_ClkDiv64();
                     }
                  }
               else
                  {
                  Timer1_set_OCRAValue(value);
                  Timer1_setClockSpeed_ClkDiv8();
                  }
               }
            else
               {
               Timer1_set_OCRAValue(value);
               Timer1_setClockSpeed_Clk();
               }
            }
         };
      }
   }

#endif /* BSP_ARCHITECTURE_AVR_TIMER_HPP_ */
