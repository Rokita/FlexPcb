/*
 * Gpio.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_ARCHITECTURE_AVR_GPIO_HPP_
#define BSP_ARCHITECTURE_AVR_GPIO_HPP_

#include <BSP/AVR/AVR_Architecture.hpp>
#include <BRD/AVR/gpio.h>

namespace Gpio
   {
   namespace PortB
      {
      class Pin_0: public AVR_Architecture::GpioOutput<eGpioPort_B, eGpioPin_0>
         {
         };
      class Pin_1: public AVR_Architecture::GpioOutput<eGpioPort_B, eGpioPin_1>
         {
         };
      class Pin_2: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_2>
         {
         };
      class Pin_3: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_3>
         {
         };
      class Pin_4: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_4>
         {
         };
      class Pin_5: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_5>
         {
         };
      class Pin_6: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_6>
         {
         };
      class Pin_7: public AVR_Architecture::GpioUnused<eGpioPort_B, eGpioPin_7>
         {
         };
      }
   namespace PortC
      {
      class Pin_0: public AVR_Architecture::GpioOutput<eGpioPort_C, eGpioPin_0>
         {
         };
      class Pin_1: public AVR_Architecture::GpioOutput<eGpioPort_C, eGpioPin_1>
         {
         };
      class Pin_2: public AVR_Architecture::GpioOutput<eGpioPort_C, eGpioPin_2>
         {
         };
      class Pin_3: public AVR_Architecture::GpioOutput<eGpioPort_C, eGpioPin_3>
         {
         };
      class Pin_4: public AVR_Architecture::GpioInput<eGpioPort_C, eGpioPin_4>
         {
         };
      class Pin_5: public AVR_Architecture::GpioInput<eGpioPort_C, eGpioPin_5>
         {
         };
      class Pin_6: public AVR_Architecture::GpioSpecial<eGpioPort_C, eGpioPin_6>
         {
         };
      }
   namespace PortD
      {
      class Pin_0: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_0>
         {
         };
      class Pin_1: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_1>
         {
         };
      class Pin_2: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_2>
         {
         };
      class Pin_3: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_3>
         {
         };
      class Pin_4: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_4>
         {
         };
      class Pin_5: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_5>
         {
         };
      class Pin_6: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_6>
         {
         };
      class Pin_7: public AVR_Architecture::GpioOutput<eGpioPort_D, eGpioPin_7>
         {
         };
      }
   }

#endif /* BSP_ARCHITECTURE_AVR_GPIO_HPP_ */
