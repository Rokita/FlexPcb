/*
 * BSP.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_HPP_
#define BSP_BSP_HPP_

extern "C"
   {
#include <stdint.h>
   }
class BSP
   {
public:
   BSP();

   void init();
   void update(uint16_t time);
   /* Buttons */
   bool Button1_isPressed();
   bool Button1_isReleased();
   bool Button2_isPressed();
   bool Button2_isReleased();
   /* Leds */
   void Led1_on();
   void Led1_off();
   void Led1_toggle();
   void Led1_blink(uint16_t onTime_ms, uint16_t offTime_ms);
   void Led2_on();
   void Led2_off();
   void Led2_toggle();
   void Led2_blink(uint16_t onTime_ms, uint16_t offTime_ms);
   void Led14_on();
   void Led14_off();
   void Led14_toggle();
   void Led14_blink(uint16_t onTime_ms, uint16_t offTime_ms);
   /* Displays */
   void DisplayTens_setSegments(uint8_t segments);
   void DisplayUnits_setSegments(uint8_t segments);
   /* Displays */
   void Buzzer_GenerateFrequency(uint32_t freq, uint16_t time);
   void Buzzer_Stop();

   /* Timers */
   void ApplicationTimer_setTime(uint16_t time);
   void ApplicationTimer_start();
   void ApplicationTimer_stop();
   void ApplicationTimer_reset();
   bool ApplicationTimer_isExpired();
   };

extern BSP oBSP;

#endif /* BSP_BSP_HPP_ */
