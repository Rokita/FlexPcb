/*
 * BSP_Dispay_7seg.hpp
 *
 *  Created on: 22.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_DISPAY_7SEG_HPP_
#define BSP_BSP_DISPAY_7SEG_HPP_

#include <BSP/BSP_led.hpp>

enum seg_bit_pos
   {
   eSegA = 0,
   eSegB,
   eSegC,
   eSegD,
   eSegE,
   eSegF,
   eSegG,
   eSegDP,
   eSeg_NumOf
   };

template<typename SEG_A, typename SEG_B, typename SEG_C, typename SEG_D, typename SEG_E, typename SEG_F, typename SEG_G,
      typename SEG_DP, typename DS_PIN>
class Display_7SEG
   {
public:
   void show();
   void hide();
   void setSegments(uint8_t segments);
private:
   uint8_t seg_bits;
   DS_PIN displaySelect;
   SEG_A ledSegA;
   SEG_B ledSegB;
   SEG_C ledSegC;
   SEG_D ledSegD;
   SEG_E ledSegE;
   SEG_F ledSegF;
   SEG_G ledSegG;
   SEG_DP ledSegDP;
   };

/*************************************
 *              DEFINITION
 *************************************/
template<typename SEG_A, typename SEG_B, typename SEG_C, typename SEG_D, typename SEG_E, typename SEG_F, typename SEG_G,
      typename SEG_DP, typename DS_PIN>
void Display_7SEG<SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G, SEG_DP, DS_PIN>::show()
   {
   displaySelect.reset();
   (seg_bits & (1 << eSegA)) ? ledSegA.reset() : ledSegA.set();
   (seg_bits & (1 << eSegB)) ? ledSegB.reset() : ledSegB.set();
   (seg_bits & (1 << eSegC)) ? ledSegC.reset() : ledSegC.set();
   (seg_bits & (1 << eSegD)) ? ledSegD.reset() : ledSegD.set();
   (seg_bits & (1 << eSegE)) ? ledSegE.reset() : ledSegE.set();
   (seg_bits & (1 << eSegF)) ? ledSegF.reset() : ledSegF.set();
   (seg_bits & (1 << eSegG)) ? ledSegG.reset() : ledSegG.set();
   (seg_bits & (1 << eSegDP)) ? ledSegDP.reset() : ledSegDP.set();
   displaySelect.set();
   }
template<typename SEG_A, typename SEG_B, typename SEG_C, typename SEG_D, typename SEG_E, typename SEG_F, typename SEG_G,
      typename SEG_DP, typename DS_PIN>
void Display_7SEG<SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G, SEG_DP, DS_PIN>::hide()
   {
   displaySelect.reset();
   }

template<typename SEG_A, typename SEG_B, typename SEG_C, typename SEG_D, typename SEG_E, typename SEG_F, typename SEG_G,
      typename SEG_DP, typename DS_PIN>
void Display_7SEG<SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G, SEG_DP, DS_PIN>::setSegments(uint8_t segments)
   {
   seg_bits = segments;
   }
#endif /* BSP_BSP_DISPAY_7SEG_HPP_ */
