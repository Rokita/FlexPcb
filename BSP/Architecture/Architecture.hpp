/*
 * Architecture.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_ARCHITECTURE_ARCHITECTURE_HPP_
#define BSP_ARCHITECTURE_ARCHITECTURE_HPP_

extern "C"
{
#include <stdint.h>
}

namespace Architecture
   {

   typedef enum GpioDirection
      {
      eGpioDirection_Input = 0,
      eGpioDirection_Output,
      eGpioDirection_LastUnused
      } GpioDirection_T;

   class Gpio
      {
   public:
      Gpio(GpioDirection_T dir = eGpioDirection_LastUnused) :
            direction(dir)
         {

         }
   protected:
      GpioDirection_T direction;
      };

   class GpioInput: public Gpio
      {
   public:
      GpioInput() :
            Gpio(eGpioDirection_Input)
         {
         }
      bool read();
      };

   class GpioOutput: public Gpio
      {
   public:
      GpioOutput() :
            Gpio(eGpioDirection_Output)
         {
         }
      void set();
      void reset();
      void toggle();
      bool isSet();
      };

   class GpioSpecial: public Gpio
      {
      };

   class GpioUnused: public Gpio
      {
      };

   class Timer
      {
      public:
      Timer()
      {

      };
      void start();
      void stop();
      void setFrequency(uint32_t freq);
      };
   }

#endif /* BSP_ARCHITECTURE_ARCHITECTURE_HPP_ */
