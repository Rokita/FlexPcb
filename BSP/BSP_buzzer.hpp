/*
 * BSP_buzzer.hpp
 *
 *  Created on: 20.03.2018
 *      Author: Kamil
 */

#ifndef BSP_BSP_BUZZER_HPP_
#define BSP_BSP_BUZZER_HPP_

template<typename BUZ_PIN, class GENERATOR>
class Buzzer
   {
public:
   Buzzer();
   void update(uint16_t time);
   void generateFrequency(uint32_t freq, uint16_t time);
   void stop();
   void toggle();

private:
   uint16_t elapsedTime;
   uint16_t generationTime;

   GENERATOR generator;
   BUZ_PIN buzzerPin;
   };

template<typename BUZ_PIN, class GENERATOR>
Buzzer<BUZ_PIN, GENERATOR>::Buzzer()
   {
   elapsedTime = 0;
   generationTime = 0;
   }

template<typename BUZ_PIN, class GENERATOR>
void Buzzer<BUZ_PIN, GENERATOR>::update(uint16_t time)
   {
   if(elapsedTime > generationTime)
      {
      elapsedTime = 0;
      generationTime = 0;
      buzzerPin.reset();
      generator.stop();
      }
   elapsedTime += time;
   }
template<typename BUZ_PIN, class GENERATOR>
void Buzzer<BUZ_PIN, GENERATOR>::generateFrequency(uint32_t freq, uint16_t time)
   {
   elapsedTime = 0;
   generationTime = time;
   /*
    *   Multiplication by 2
    *   Buzzer has to change his state twice per period
    */
   generator.setFrequency(2*freq);
   generator.start();
   }

template<typename BUZ_PIN, class GENERATOR>
void Buzzer<BUZ_PIN, GENERATOR>::stop()
   {
   elapsedTime = 0;
   generationTime = 0;
   }
template<typename BUZ_PIN, class GENERATOR>
void Buzzer<BUZ_PIN, GENERATOR>::toggle()
   {
   buzzerPin.toggle();
   }

#endif /* BSP_BSP_BUZZER_HPP_ */
