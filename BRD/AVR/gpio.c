/*
 * gpio.c
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#include "gpio.h"
#include <avr/io.h>

volatile uint8_t * const INPUT_REGISTER[eGpioPort_NumOf] =
   {
   &PINB, &PINC, &PIND,
   };

volatile uint8_t * OUTPUT_REGISTER[eGpioPort_NumOf] =
   {
   &PORTB, &PORTC, &PORTD
   };

volatile uint8_t * DIR_REGISTER[eGpioPort_NumOf] =
   {
   &DDRB, &DDRC, &DDRD
   };

#define GET_REG_IN(port)        (*(INPUT_REGISTER[port]))
#define GET_REG_OUT(port)       (*(OUTPUT_REGISTER[port]))
#define GET_REG_DIR(port)       (*(DIR_REGISTER[port]))

#define PIN_INPUT(port, pin)    (GET_REG_DIR(port) &= ~(1 << pin))
#define PIN_OUTPUT(port, pin)   (GET_REG_DIR(port) |= (1 << pin))
#define PIN_PULLUP(port, pin)   (GET_REG_OUT(port) |= (1 << pin))
#define PIN_NOPULLUP(port, pin) (GET_REG_OUT(port) &= ~(1 << pin))
#define PIN_READ(port, pin)     (GET_REG_IN(port) & (1 << pin))
#define PIN_SET(port, pin)      (GET_REG_OUT(port) |= (1 << pin))
#define PIN_RESET(port, pin)    (GET_REG_OUT(port) &= ~(1 << pin))
#define PIN_TOGGLE(port, pin)   (GET_REG_OUT(port) ^= (1 << pin))
/***************************************************************
 *
 ***************************************************************/

void Gpio_Pin_Config_Input(GpioPort_T port, GpioPin_T pin)
   {
   PIN_INPUT(port, pin);
   }
void Gpio_Pin_Config_Output(GpioPort_T port, GpioPin_T pin)
   {
   PIN_OUTPUT(port, pin);
   }

void Gpio_Pin_PullUp(GpioPort_T port, GpioPin_T pin)
   {
   PIN_PULLUP(port, pin);
   }
void Gpio_Pin_NoPullUp(GpioPort_T port, GpioPin_T pin)
   {
   PIN_NOPULLUP(port, pin);
   }

GpioPinState_T Gpio_Pin_Read(GpioPort_T port, GpioPin_T pin)
   {
   return (PIN_READ(port, pin) ? eGpioPinState_High : eGpioPinState_Low);
   }
void Gpio_Pin_Set(GpioPort_T port, GpioPin_T pin)
   {
   PIN_SET(port, pin);
   }
void Gpio_Pin_Reset(GpioPort_T port, GpioPin_T pin)
   {
   PIN_RESET(port, pin);
   }
void Gpio_Pin_Toggle(GpioPort_T port, GpioPin_T pin)
   {
   PIN_TOGGLE(port, pin);
   }
