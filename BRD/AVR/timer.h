/*
 * timer.h
 *
 *  Created on: 25.03.2018
 *      Author: Kamil
 */

#ifndef BRD_AVR_TIMER_H_
#define BRD_AVR_TIMER_H_

#ifdef __cplusplus
extern "C"
   {
#endif

#include <stdint.h>

typedef void (*FunctionPointer )(void);

void Timer0_setCompAMode_Disconected();
void Timer0_setCompAMode_Toggle();
void Timer0_setCompAMode_Clear();
void Timer0_setCompAMode_Set();

void Timer0_setCompBMode_Disconected();
void Timer0_setCompBMode_Toggle();
void Timer0_setCompBMode_Clear();
void Timer0_setCompBMode_Set();

void Timer0_setMode_Normal();
void Timer0_setMode_PwmPhaseCorrectTop();
void Timer0_setMode_CTC();
void Timer0_setMode_FastPwmTop();
void Timer0_setMode_PwmPhaseCorrectOCRA();
void Timer0_setMode_FasePwmOCRA();

void Timer0_enableInterrupt_CompA();
void Timer0_enableInterrupt_CompB();
void Timer0_enableInterrupt_Overflow();
void Timer0_disableInterrupt_CompA();
void Timer0_disableInterrupt_CompB();
void Timer0_disableInterrupt_Overflow();

void Timer0_setInterruptionCompA_Callback(FunctionPointer pFun);
void Timer0_setInterruptionCompB_Callback(FunctionPointer pFun);
void Timer0_setInterruptionOverflow_Callback(FunctionPointer pFun);

void Timer0_set_OCRAValue(uint8_t val);
void Timer0_set_OCRBValue(uint8_t val);

void Timer0_setClockSpeed_None();
void Timer0_setClockSpeed_Clk();
void Timer0_setClockSpeed_ClkDiv8();
void Timer0_setClockSpeed_ClkDiv64();
void Timer0_setClockSpeed_ClkDiv256();
void Timer0_setClockSpeed_ClkDiv1024();
void Timer0_setClockSpeed_ExtFalling();
void Timer0_setClockSpeed_ExtRising();

/*
 *
 * Timer 1
 *
 */

void Timer1_setCompAMode_Disconected();
void Timer1_setCompAMode_Toggle();
void Timer1_setCompAMode_Clear();
void Timer1_setCompAMode_Set();

void Timer1_setCompBMode_Disconected();
void Timer1_setCompBMode_Toggle();
void Timer1_setCompBMode_Clear();
void Timer1_setCompBMode_Set();

void Timer1_setMode_CTC();

void Timer1_enableInterrupt_CompA();
void Timer1_enableInterrupt_CompB();
void Timer1_enableInterrupt_Overflow();
void Timer1_enableInterrupt_InputCapture();
void Timer1_disableInterrupt_CompA();
void Timer1_disableInterrupt_CompB();
void Timer1_disableInterrupt_Overflow();
void Timer1_disableInterrupt_InputCapture();

void Timer1_setInterruptionCompA_Callback(FunctionPointer pFun);
void Timer1_setInterruptionCompB_Callback(FunctionPointer pFun);
void Timer1_setInterruptionOverflow_Callback(FunctionPointer pFun);
void Timer1_setInterruptionInputCapture_Callback(FunctionPointer pFun);

void Timer1_set_OCRAValue(uint16_t val);
void Timer1_set_OCRBValue(uint16_t val);

void Timer1_setClockSpeed_None();
void Timer1_setClockSpeed_Clk();
void Timer1_setClockSpeed_ClkDiv8();
void Timer1_setClockSpeed_ClkDiv64();
void Timer1_setClockSpeed_ClkDiv256();
void Timer1_setClockSpeed_ClkDiv1024();
void Timer1_setClockSpeed_ExtFalling();
void Timer1_setClockSpeed_ExtRising();

#ifdef __cplusplus
   }
#endif
#endif /* BRD_AVR_TIMER_H_ */
