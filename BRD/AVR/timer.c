/*
 * timer.c
 *
 *  Created on: 25.03.2018
 *      Author: Kamil
 */

#include "timer.h"
#include <avr/io.h>
#include <avr/interrupt.h>

volatile FunctionPointer  Tim0_OCRA_Interrupt = (FunctionPointer ) 0;
volatile FunctionPointer  Tim0_OCRB_Interrupt = (FunctionPointer ) 0;
volatile FunctionPointer  Tim0_Overflow_Interrupt = (FunctionPointer ) 0;

volatile FunctionPointer  Tim1_OCRA_Interrupt = (FunctionPointer ) 0;
volatile FunctionPointer  Tim1_OCRB_Interrupt = (FunctionPointer ) 0;
volatile FunctionPointer  Tim1_Overflow_Interrupt = (FunctionPointer ) 0;
volatile FunctionPointer  Tim1_InputCapture_Interrupt = (FunctionPointer ) 0;

void clearTC0CompAModeBits()
   {
   TCCR0A &= ~(1 << COM0A0);
   TCCR0A &= ~(1 << COM0A1);
   }

void Timer0_setCompAMode_Disconected()
   {
   clearTC0CompAModeBits();
   }
void Timer0_setCompAMode_Toggle()
   {
   clearTC0CompAModeBits();
   TCCR0A |= (1 << COM0A0);
   }
void Timer0_setCompAMode_Clear()
   {
   clearTC0CompAModeBits();
   TCCR0A |= (1 << COM0A1);
   }
void Timer0_setCompAMode_Set()
   {
   clearTC0CompAModeBits();
   TCCR0A |= (1 << COM0A0);
   TCCR0A |= (1 << COM0A1);
   }

void clearTC0CompBModeBits()
   {
   TCCR0A &= ~(1 << COM0B0);
   TCCR0A &= ~(1 << COM0B1);
   }

void Timer0_setCompBMode_Disconected()
   {
   clearTC0CompBModeBits();
   }
void Timer0_setCompBMode_Toggle()
   {
   clearTC0CompBModeBits();
   TCCR0A |= (1 << COM0B0);
   }
void Timer0_setCompBMode_Clear()
   {
   clearTC0CompBModeBits();
   TCCR0A |= (1 << COM0B1);
   }
void Timer0_setCompBMode_Set()
   {
   clearTC0CompBModeBits();
   TCCR0A |= (1 << COM0B0);
   TCCR0A |= (1 << COM0B1);
   }

void clearTC0ModeBits()
   {
   Timer0_setClockSpeed_None();
   TCCR0A &= ~(1 << WGM00);
   TCCR0A &= ~(1 << WGM01);
   TCCR0B &= ~(1 << WGM02);
   }

void Timer0_setMode_Normal()
   {
   clearTC0ModeBits();
   }
void Timer0_setMode_PwmPhaseCorrectTop()
   {
   clearTC0ModeBits();
   TCCR0A |= (1 << WGM00);
   }
void Timer0_setMode_CTC()
   {
   clearTC0ModeBits();
   TCCR0A |= (1 << WGM01);
   }
void Timer0_setMode_FastPwmTop()
   {
   clearTC0ModeBits();
   TCCR0A |= (1 << WGM00);
   TCCR0A |= (1 << WGM01);
   }
void Timer0_setMode_PwmPhaseCorrectOCRA()
   {
   clearTC0ModeBits();
   TCCR0A |= (1 << WGM00);
   TCCR0B |= (1 << WGM02);
   }
void Timer0_setMode_FasePwmOCRA()
   {
   clearTC0ModeBits();
   TCCR0A |= (1 << WGM00);
   TCCR0A |= (1 << WGM01);
   TCCR0B |= (1 << WGM02);
   }

void Timer0_enableInterrupt_CompA()
   {
   TIMSK0 |= (1 << OCIE0A);
   }
void Timer0_enableInterrupt_CompB()
   {
   TIMSK0 |= (1 << OCIE0B);
   }
void Timer0_enableInterrupt_Overflow()
   {
   TIMSK0 |= (1 << TOIE0);
   }

void Timer0_disableInterrupt_CompA()
   {
   TIMSK0 &= ~(1 << OCIE0A);
   }
void Timer0_disableInterrupt_CompB()
   {
   TIMSK0 &= ~(1 << OCIE0B);
   }
void Timer0_disableInterrupt_Overflow()
   {
   TIMSK0 &= ~(1 << TOIE0);
   }

void Timer0_setInterruptionCompA_Callback(FunctionPointer  pFun)
   {
   Tim0_OCRA_Interrupt = pFun;
   }
void Timer0_setInterruptionCompB_Callback(FunctionPointer  pFun)
   {
   Tim0_OCRB_Interrupt = pFun;
   }
void Timer0_setInterruptionOverflow_Callback(FunctionPointer  pFun)
   {
   Tim0_Overflow_Interrupt = pFun;
   }

void Timer0_set_OCRAValue(uint8_t val)
   {
   OCR0A = val;
   }
void Timer0_set_OCRBValue(uint8_t val)
   {
   OCR0B = val;
   }

void clearTC0SpeedBits()
   {
   TCCR0B &= ~(1 << CS00);
   TCCR0B &= ~(1 << CS01);
   TCCR0B &= ~(1 << CS02);
   }

void Timer0_setClockSpeed_None()
   {
   clearTC0SpeedBits();
   }
void Timer0_setClockSpeed_Clk()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS00);
   }
void Timer0_setClockSpeed_ClkDiv8()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS01);
   }
void Timer0_setClockSpeed_ClkDiv64()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS00);
   TCCR0B |= (1 << CS01);
   }
void Timer0_setClockSpeed_ClkDiv256()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS02);
   }
void Timer0_setClockSpeed_ClkDiv1024()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS00);
   TCCR0B |= (1 << CS02);
   }
void Timer0_setClockSpeed_ExtFalling()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS01);
   TCCR0B |= (1 << CS02);
   }
void Timer0_setClockSpeed_ExtRising()
   {
   clearTC0SpeedBits();
   TCCR0B |= (1 << CS00);
   TCCR0B |= (1 << CS01);
   TCCR0B |= (1 << CS02);
   }
/*
 *
 *
 * Timer 1
 *
 *
 */
void clearTC1CompAModeBits()
   {
   TCCR1A &= ~(1 << COM1A0);
   TCCR1A &= ~(1 << COM1A1);
   }

void Timer1_setCompAMode_Disconected()
   {
   clearTC1CompAModeBits();
   }
void Timer1_setCompAMode_Toggle()
   {
   clearTC1CompAModeBits();
   TCCR1A |= (1 << COM1A0);
   }
void Timer1_setCompAMode_Clear()
   {
   clearTC1CompAModeBits();
   TCCR1A |= (1 << COM1A1);
   }
void Timer1_setCompAMode_Set()
   {
   clearTC1CompAModeBits();
   TCCR1A |= (1 << COM1A0);
   TCCR1A |= (1 << COM1A1);
   }

void clearTC1CompBModeBits()
   {
   TCCR1A &= ~(1 << COM1B0);
   TCCR1A &= ~(1 << COM1B1);
   }

void Timer1_setCompBMode_Disconected()
   {
   clearTC1CompBModeBits();
   }
void Timer1_setCompBMode_Toggle()
   {
   clearTC1CompBModeBits();
   TCCR1A |= (1 << COM1B0);
   }
void Timer1_setCompBMode_Clear()
   {
   clearTC1CompBModeBits();
   TCCR1A |= (1 << COM1B1);
   }
void Timer1_setCompBMode_Set()
   {
   clearTC1CompBModeBits();
   TCCR1A |= (1 << COM1B0);
   TCCR1A |= (1 << COM1B1);
   }
void clearTC1ModeBits()
   {
   Timer1_setClockSpeed_None();
   TCCR1A &= ~(1 << WGM10);
   TCCR1A &= ~(1 << WGM11);
   TCCR1B &= ~(1 << WGM12);
   TCCR1B &= ~(1 << WGM13);
   }
void Timer1_setMode_CTC()
   {
   clearTC1ModeBits();
   TCCR1B |= (1 << WGM12);
   }



/*
 * Missed some modes
 */
void Timer1_enableInterrupt_CompA()
   {
   TIMSK1 |= (1 << OCIE1A);
   }
void Timer1_enableInterrupt_CompB()
   {
   TIMSK1 |= (1 << OCIE1B);
   }
void Timer1_enableInterrupt_Overflow()
   {
   TIMSK1 |= (1 << TOIE1);
   }
void Timer1_enableInterrupt_InputCapture()
   {
   TIMSK1 |= (1 << ICIE1);
   }

void Timer1_disableInterrupt_CompA()
   {
   TIMSK1 &= ~(1 << OCIE1A);
   }
void Timer1_disableInterrupt_CompB()
   {
   TIMSK1 &= ~(1 << OCIE1B);
   }
void Timer1_disableInterrupt_Overflow()
   {
   TIMSK1 &= ~(1 << TOIE1);
   }

void Timer1_disableInterrupt_InputCapture()
   {
   TIMSK1 &= ~(1 << ICIE1);
   }

void Timer1_setInterruptionCompA_Callback(FunctionPointer  pFun)
   {
   Tim1_OCRA_Interrupt = pFun;
   }
void Timer1_setInterruptionCompB_Callback(FunctionPointer  pFun)
   {
   Tim1_OCRB_Interrupt = pFun;
   }
void Timer1_setInterruptionOverflow_Callback(FunctionPointer  pFun)
   {
   Tim1_Overflow_Interrupt = pFun;
   }
void Timer1_setInterruptionInputCapture_Callback(FunctionPointer  pFun)
   {
   Tim1_InputCapture_Interrupt = pFun;
   }

void clearTC1SpeedBits()
   {
   TCCR1B &= ~(1 << CS10);
   TCCR1B &= ~(1 << CS11);
   TCCR1B &= ~(1 << CS12);
   }

void Timer1_set_OCRAValue(uint16_t val)
   {
   OCR1A = val;
   }
void Timer1_set_OCRBValue(uint16_t val)
   {
   OCR1B = val;
   }
void Timer1_setClockSpeed_None()
   {
   clearTC1SpeedBits();
   }
void Timer1_setClockSpeed_Clk()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS10);
   }
void Timer1_setClockSpeed_ClkDiv8()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS11);
   }
void Timer1_setClockSpeed_ClkDiv64()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS10);
   TCCR1B |= (1 << CS11);
   }
void Timer1_setClockSpeed_ClkDiv256()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS12);
   }
void Timer1_setClockSpeed_ClkDiv1024()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS10);
   TCCR1B |= (1 << CS12);
   }
void Timer1_setClockSpeed_ExtFalling()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS11);
   TCCR1B |= (1 << CS12);
   }
void Timer1_setClockSpeed_ExtRising()
   {
   clearTC1SpeedBits();
   TCCR1B |= (1 << CS10);
   TCCR1B |= (1 << CS11);
   TCCR1B |= (1 << CS12);
   }
