/*
 * gpio.h
 *
 *  Created on: 21.03.2018
 *      Author: Kamil
 */

#ifndef BRD_AVR_GPIO_H_
#define BRD_AVR_GPIO_H_

#ifdef __cplusplus
extern "C"
   {
#endif

   typedef enum GpioPort
      {
      eGpioPort_B = 0,
      eGpioPort_C,
      eGpioPort_D,
      eGpioPort_NumOf
      } GpioPort_T;
   typedef enum GpioPin
      {
      eGpioPin_0 = 0,
      eGpioPin_1,
      eGpioPin_2,
      eGpioPin_3,
      eGpioPin_4,
      eGpioPin_5,
      eGpioPin_6,
      eGpioPin_7,
      eGpioPin_NumOf
      } GpioPin_T;

   typedef enum GpioPinState
      {
      eGpioPinState_Low = 0,
      eGpioPinState_High,
      eGpioPinState_NumOf
      } GpioPinState_T;

   void Gpio_Pin_Config_Input(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_Config_Output(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_PullUp(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_NoPullUp(GpioPort_T port, GpioPin_T pin);
   GpioPinState_T Gpio_Pin_Read(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_Set(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_Reset(GpioPort_T port, GpioPin_T pin);
   void Gpio_Pin_Toggle(GpioPort_T port, GpioPin_T pin);

#ifdef __cplusplus
   }
#endif

#endif /* BRD_AVR_GPIO_H_ */
